# netplan.io

This cookbook install and configure the Ubuntu [netplan](http://netplan.io) 
package.

In order to prevent from breaking the initial netplan configuration file from 
the node you're bootstrapping, this cookbook update the netplan configuration
file by matching the device name like:

```ruby
# Policyfile

name: 'my-policy'

# ...

default['netplan.io'] = {
  ethernets: {
    device: 'ens3'
    // your options here
  }
}

```

It prevents the Chef changes history feature from working but it also prevents
from breaking your config, sort of tradeoff.

## Supported features are:

* **Blind configuration**: You can configure the first and single device without knowing its name. [Read more](docs/blind_configuration.md)
* **Global configuration**: You can configure all the devices no matter their name. [Read more](docs/global_configuration.md)

## Supported Platforms

* Ubuntu

## Attributes

Please note that all attributes are optional.

| Key | Type | Description | Default |
|-----|------|-------------|---------|
| `default['netplan.io'][:version]` | String | APT package version to be installed | `latest` |
| `default['netplan.io'][:renderer]` | String | Set a specific network renderer | `nil` |

## Usage

 - If you use it from a Policyfile:

```ruby
name 'my-policy'

default_source :supermarket

run_list 'recipe[netplan.io]'

cookbook 'netplan.io', '~> 0.1.0'

default['netplan.io'] = {
  version: 'latest'
}
```

 - If you want to use it within a cookbook :
```ruby
include_recipe 'netplan.io'
```
