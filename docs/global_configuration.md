# Feature: Global Configuration

You can configure the all the interfaces without knowing its name.

This feature is useful in the case you need to apply the same configuration to
all of the machine's network interfaces.

## An example

Given a fresh new Ubuntu machine with the following `/etc/netplan/50-cloud-init.yaml`:

```yaml
# This file is generated from information provided by the datasource.  Changes
# to it will not persist across an instance reboot.  To disable cloud-init's
# network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
network:
    version: 2
    ethernets:
        eth0:
            addresses:
            - 123.123.123.123/23
            gateway4: 123.123.123.123
            match:
                macaddress: fa:16:3e:f5:09:19
            nameservers:
                addresses:
                - 11.22.33.44
                - 11.22.44.55
                search:
                - invalid
            set-name: eth0
        eth1:
            addresses:
            - 111.222.333.444/23
            gateway4: 111.222.333.444
            match:
                macaddress: fa:16:3e:f5:09:19
            nameservers:
                addresses:
                - 11.22.33.44
                - 11.22.44.55
                search:
                - invalid
            set-name: eth1
```

And you want to update both network interfaces with different
`nameservers.addresses` and `nameservers.search`, here is how to write
your `Policyfile`:

```ruby
name 'my-policy'

default_source :supermarket

run_list 'recipe[netplan.io]'

cookbook 'netplan.io', '~> 0.3.0'

default['netplan.io'] = {
  ethernets: [
    {
      device: ':each:',
      nameservers: {
        search: %w[myorg.co],
        addresses: %w[8.8.8.8]
      }
    }
  ],
  renderer: 'networkd'
}
```

Which will produce:

```yaml
network:
    ethernets:
        eth0:
            addresses:
            - 123.123.123.123/23
            gateway4: 123.123.123.123
            match:
                macaddress: fa:16:3e:f5:09:19
            nameservers:
                addresses:
                - 8.8.8.8
                search:
                - myorg.co
            set-name: eth0
        eth1:
            addresses:
            - 111.222.333.444/23
            gateway4: 111.222.333.444
            match:
                macaddress: fa:16:3e:f5:09:19
            nameservers:
                addresses:
                - 8.8.8.8
                search:
                - myorg.co
            set-name: eth1
    version: 2
```
