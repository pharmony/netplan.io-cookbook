# Feature: Blind Configuration

You can configure the first and single device interface without knowing its name.

This feature is useful in the case you get a new Ubuntu node with a single 
network interface, and you want to ensure it always has a given name 
(like `eth0` for example), no matter the name it had initially.

This feature will raise an exception in the case the netplan configuration file
has more than one ethernet entry, or if you have multiple files defining the
same devise (network, bridge, wifi and so on).

## An example

Given a fresh new Ubuntu machine with the following `/etc/netplan/50-cloud-init.yaml`:

```yaml
# This file is generated from information provided by the datasource.  Changes
# to it will not persist across an instance reboot.  To disable cloud-init's
# network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
network:
    ethernets:
        ens3:
            dhcp4: true
            match:
                macaddress: fa:16:3e:f5:09:19
            set-name: ens3
    version: 2
```

And you want to rename that `ens3` interface into `eth0`, here is how to write 
your `Policyfile`:

```ruby
name 'my-policy'

default_source :supermarket

run_list 'recipe[netplan.io]'

cookbook 'netplan.io', '~> 0.1.0'

default['netplan.io'] = {
  ethernets: [
    {
      device: ':first:',
      set-name: 'eth0'
    }
  ]
}
```

Which will produce:

```yaml
# This file is generated from information provided by the datasource.  Changes
# to it will not persist across an instance reboot.  To disable cloud-init's
# network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
network:
    ethernets:
        ens3:
            dhcp4: true
            match:
                macaddress: fa:16:3e:f5:09:19
            set-name: eth0
    version: 2
```
