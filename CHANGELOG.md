# netplan.io CHANGELOG

This file is used to list changes made in each version of the netplan.io cookbook.

## Unreleased

## 0.4.0

- Implements global configuration feature

## 0.3.0

- Prevents from writing empty configuration file

## 0.2.0

- Ignores config files declaring empty devices groups

## 0.1.0

Initial release.
