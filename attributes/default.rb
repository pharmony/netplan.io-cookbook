# frozen_string_literal: true

# APT netplan package version to be installed
default['netplan.io'][:version] = 'latest'

# [Optional] Netplan renderer
#
# Use the given networking backend for this definition. Currently supported are
# networkd and NetworkManager. This property can be specified globally
# in network:, for a device type (in e. g. ethernets:) or
# for a particular device definition. Default is networkd.

# (Since 0.99) The renderer property has one additional acceptable value for vlan
# objects (i. e. defined in vlans:): sriov. If a vlan is defined with the
# sriov renderer for an SR-IOV Virtual Function interface, this causes netplan to
# set up a hardware VLAN filter for it. There can be only one defined per VF.
default['netplan.io'][:renderer] = nil
