# frozen_string_literal: true

module NetplanIo
  module Helpers # rubocop:disable Style/Documentation
    def cast_to_raw_objects(object)
      case object
      when Chef::Node::ImmutableArray, Chef::Node::AttrArray
        object.to_a.map { |value| cast_to_raw_objects(value) }
      when Chef::Node::ImmutableMash, Chef::Node::VividMash
        object.to_h.transform_values { |value| cast_to_raw_objects(value) }
      when Hash
        object.transform_values { |value| cast_to_raw_objects(value) }
      else object
      end
    end
  end
end
