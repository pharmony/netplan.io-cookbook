# Testing the netplan.io cookbook

This cookbook uses the Kitchen CI in order to run the tests.
All the configuration can be found in the `.kitchen.yml` (Dokken driver) file.

## First time

You need to install the dependencies in order to get kitchen drivers availables.

From this repo's folder:

```
$ bundle
```

## Usage

### Integration tests

You can list the Kitchens :

```
$ bundle exec kitchen list
Instance             Driver  Provisioner  Verifier  Transport  Last Action    Last Error
default-ubuntu-1804  Dokken  Dokken       Inspec    Dokken     <Not Created>  <None>
...
```

To run the tests, simply run `bundle exec kitchen test` :

```
$ bundle exec kitchen test
-----> Starting Test Kitchen (v2.10.0)
-----> Cleaning up any prior instances of <default-ubuntu-1804>
-----> Destroying <default-ubuntu-1804>...
...
-----> Verifying <default-ubuntu-1804>...
$$$$$$ kitchen-inspec: skipping `plugin_config` which requires InSpec version 4.26.2 or higher. Your version: 4.24.8
       Loaded tests from {:path=>".Users.guillaumehain.Developments.netplan.io.test.integration.default"}

Profile: tests from {:path=>"/Users/guillaumehain/Developments/netplan.io/test/integration/default"} (tests from {:path=>".Users.guillaumehain.Developments.netplan.io.test.integration.default"})
Version: (not specified)
Target:  docker://7e403740568860a77a5393e7ebc924ee7f876dc5c24ffde92792562b34970cae

  System Package netplan
     ✔  is expected to be installed

Test Summary: 1 successful, 0 failures, 0 skipped
       Finished verifying <default-ubuntu-1804> (0m3.66s).
...
```
