# frozen_string_literal: true

#
# Cookbook:: netplan.io
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.
require_relative '../libraries/deep_merge'

# rubocop:disable Lint/SendWithMixinArgument
# `cast_to_raw_objects`
Chef::Resource::RubyBlock.send(:include, NetplanIo::Helpers)
# rubocop:enable Lint/SendWithMixinArgument

PHYSICAL_DEVICES = %w[ethernets modems wifis].freeze
VIRTUAL_DEVICES = %w[bridges bonds tunnels vlans nm-devices].freeze

package 'netplan.io' do
  action :install
end

ruby_block 'prepare' do
  action :run
  block do
    #
    # This `update` attribute is an Array of Arrays of files to be updated.
    #
    # Each following ruby_blocks adds updates, and a final ruby_block will write
    # does updates.
    #
    node.run_state['updates'] = {}
  end
end

ruby_block 'load netplan configuration files' do
  action :run
  block do
    node.run_state['originals'] = {}

    Dir.glob('/etc/netplan/*.{yml,yaml}').each do |config_path|
      node.run_state['originals'][config_path] = YAML.load_file(config_path)
    end
  end
  not_if { Dir.glob('/etc/netplan/*.yaml').empty? }
end

ruby_block 'top-level configuration' do
  action :run
  block do
    node.run_state['originals'].each do |filepath, content|
      updates = {}

      # Ensures the 'version' field is present and has the '2' value, otherwise
      # adds it to the update queue.
      #
      # See https://netplan.io/reference/#general-structure
      content['network']['version'] != 2 && updates['version'] = 2

      if node['netplan.io']['renderer'].nil? ||
         node['netplan.io']['renderer'].empty?
        # Merge will replace the top level options, losing the version if not
        # specified.
        updates['version'] = 2
      end

      next if updates.empty?

      (node['netplan.io']['renderer'].nil? ||
       node['netplan.io']['renderer'].empty?) ||
        updates['renderer'] ||= node['netplan.io']['renderer']

      node.run_state['updates'][filepath] ||= {}
      node.run_state['updates'][filepath]['network'] = {}

      node.run_state['updates'][filepath] =
        node.run_state['updates'][filepath]['network'].merge(
          'network' => cast_to_raw_objects(updates)
        )
    end
  end
end

ruby_block 'devices configuration' do # rubocop:disable Metrics/BlockLength
  action :run
  block do # rubocop:disable Metrics/BlockLength
    interface_names = node['network']['interfaces'].keys - ['lo']
    devise_interface_count = interface_names.size

    node.run_state['originals'].each do |filepath, content| # rubocop:disable Metrics/BlockLength
      (PHYSICAL_DEVICES + VIRTUAL_DEVICES).each do |physical_device| # rubocop:disable Metrics/BlockLength
        next unless node['netplan.io'].key?(physical_device)

        # Which device should we update?
        node['netplan.io'][physical_device].each_with_index do |configuration, index| # rubocop:disable Metrics/BlockLength
          # When a device configuration is invalid (missing the `device`
          # attribute), skip it.
          next if configuration['device'].nil? || configuration['device'].empty?

          if configuration['device'] == ':each:' &&
             configuration.key?('set-name')
            Chef::Log.error 'netplan: You combined :each: with set-name for ' \
                            "devise named #{physical_device} which is not " \
                            'supported.'

            next
          end

          # Builds the list of devices to be updated.
          devices = if configuration['device'] == ':first:' && index.zero?
                      # Device name used to insert or update config.
                      # Blind renaming feature: when the value is `:first:` then
                      #                         it takes the first net device
                      #                         from config file.
                      device = content['network'][physical_device]&.first&.first

                      if devise_interface_count > 1
                        interface_names = node['network'][
                          'interfaces'
                        ].keys.join(', ')

                        Chef::Log.warn "netplan WARNING: You've enabled the " \
                                       'blind renaming feature, and this ' \
                                       "server has #{devise_interface_count} " \
                                       'network interfaces. Tnis cookbook ' \
                                       'will use the interface named ' \
                                       "#{device_net}, but be aware that " \
                                       'this machine has the following ' \
                                       "network interfaces: #{interface_names}"
                      end

                      if device_net.nil? && configuration.key?('set-name')
                        Chef::Log.warn 'netplan: You requested to rename the ' \
                                       "first #{physical_device} device as " \
                                       "#{configuration['set-name']} but " \
                                       "there's no #{physical_device} " \
                                       'devices defined.'

                        next
                      end

                      device
                    elsif configuration['device'] == ':each:'
                      interface_names
                    else
                      configuration['device']
                    end

          devices.each do |device_net| # rubocop:disable Metrics/BlockLength
            updates = {}

            should_populate_updates = content['network'][physical_device]&.key?(
              configuration['device']
            ) || (
              configuration['device'] == ':first:' && index.zero?
            ) || configuration['device'] == ':each:'

            # Updates the device configuration when the netplan configuration
            # file has an entry for the configuration device, or when the blind
            # renaming feature is enabled and the current device is the first
            # one, or when the global configuration feature is enabled.
            if should_populate_updates

              # Current device configuration from /etc/netplan/ folder
              net = case configuration['device']
                    when ':first:'
                      conf = content['network'][physical_device].first
                      { conf.first => conf.last }
                    when ':each:'
                      content['network'][physical_device][device_net]
                    else
                      content['network'][physical_device][
                        configuration['device']
                      ]
                    end

              configuration.keys.each do |property|
                next if property == 'device'

                if net.key?(property)
                  updates[property] = content['network'][physical_device][
                    device_net
                  ][property]
                  updates[property].merge!(
                    cast_to_raw_objects(configuration[property])
                  )
                else
                  updates[property] = cast_to_raw_objects(
                    configuration[property]
                  )
                end
              end
            end

            next if updates.empty?

            node.run_state['updates'][filepath] ||= cast_to_raw_objects(content)
            node.run_state['updates'][filepath]['network'] ||=
              cast_to_raw_objects(content['network']) || {}
            node.run_state['updates'][filepath]['network'][physical_device] ||=
              cast_to_raw_objects(content['network'][physical_device]) || {}
            node.run_state['updates'][filepath]['network'][physical_device][
              device_net
            ] ||= cast_to_raw_objects(
              content['network'][physical_device][device_net]
            )
            node.run_state['updates'][filepath] =
              node.run_state['updates'][filepath].deep_merge(
                'network' => { physical_device => { device_net => updates } }
              )
            node.run_state['updates'][filepath]['network'][physical_device][
              device_net
            ] = node.run_state['updates'][filepath]['network'][
              physical_device][device_net].deep_merge(
                updates
              )

            # Blind renaming feature: when the value is `:first:` then copies
            # the old interface with the new name and delete it.
            if configuration['device'] == ':first:' &&
               configuration.key?('set-name')
              node.run_state['updates'][filepath]['network'][physical_device][
                configuration['set-name']
              ] = node.run_state['updates'][filepath]['network'][
                physical_device
              ][device_net]
              node.run_state['updates'][filepath]['network'][
                physical_device
              ].delete(device_net)
            elsif configuration['device'] == ':each:'
              # Nothing more to do than `node.run_state` update made from
              # previous lines
            else
              node.run_state['updates'][filepath]['network'][
                physical_device
              ][device_net] = cast_to_raw_objects(
                configuration.except('device')
              )
            end
          end
        end
      end
    end
  end

  not_if do
    node['netplan.io'].keys.select do |key|
      (PHYSICAL_DEVICES + VIRTUAL_DEVICES).include?(key)
    end.empty?
  end
end

ruby_block 'write updated configuration files' do # rubocop:disable Metrics/BlockLength
  action :run
  block do
    node.run_state['updates'].each do |filepath, content|
      devices_keys = content['network'].keys.select do |key|
        (PHYSICAL_DEVICES + VIRTUAL_DEVICES).include?(key)
      end

      if devices_keys.empty?
        Chef::Log.warn "netplan: Skipping updating the file #{filepath} " \
                       'otherwise it would have been emptied!'
        next
      end

      if devices_keys.detect { |key| content['network'][key].empty? }
        Chef::Log.warn "netplan: Skipping updating the file #{filepath} " \
                       'otherwise it would have at least one empty device!'
        next
      end

      content['network'] = content['network'].sort.to_h

      # netplan.io has a bug which makes it parsing twice the config, which
      # makes `netplan generate` to raise the following error :
      # > Duplicate access point SSID
      #
      # Moving the `bridges` device to the end fix that.
      #
      # See https://bugs.launchpad.net/ubuntu/+source/netplan.io/+bug/1809994
      if content['network'].key?('bridges')
        bridges = content['network'].delete('bridges')
        content['network']['bridges'] = bridges
      end

      File.open(filepath, 'w') do |file|
        YAML.dump(content, file, indentation: 4)
      end

      puts "#{filepath} : #{File.read(filepath).inspect}"

      node.run_state['updated_config'] = true
    end
  end

  not_if do
    node.run_state['updates'].empty?
  end
end

execute 'netplan generate' do
  action :run
  command 'netplan generate'

  only_if { node.run_state['updated_config'] }
end

execute 'netplan apply' do
  action :run
  command 'netplan apply'

  only_if { node.run_state['updated_config'] }
end
