# frozen_string_literal: true

# Chef InSpec test for recipe netplan.io::default

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

# The netplan package should be installed
describe package('netplan.io') do
  it { should be_installed }
end

describe file('/etc/netplan/66-nets-setup.yaml') do # rubocop:disable Metrics/BlockLength
  it { should exist }
  its('content') do # rubocop:disable Metrics/BlockLength
    should match(
      <<~NETPLAN
        ---
        network:
            ethernets:
                id0:
                    dhcp4: true
                    match:
                        macaddress: 00:11:22:33:44:55
                    set-name: id0
                    wakeonlan: true
                    addresses:
                    - 192.168.14.2/24
                    - 192.168.14.3/24
                    - 2001:1::1/64
                    nameservers:
                        search:
                        - foo.local
                        - bar.local
                        addresses:
                        - 8.8.8.8
                    routes:
                    -   to: 0.0.0.0/0
                        via: 192.168.14.1
                    -   to: "::/0"
                        via: 2001:1::2
                    -   to: 0.0.0.0/0
                        via: 11.0.0.1
                        table: 70
                        on-link: true
                        metric: 3
                    routing-policy:
                    -   to: 10.0.0.0/8
                        from: 192.168.14.2/24
                        table: 70
                        priority: 100
                    -   to: 20.0.0.0/8
                        from: 192.168.14.3/24
                        table: 70
                        priority: 50
                    renderer: networkd
                lom:
                    match:
                        driver: ixgbe
                    set-name: lom1
                    dhcp6: true
                switchports:
                    match:
                        name: enp2*
                    mtu: 1280
                    renderer: networkd
            renderer: NetworkManager
            version: 2
            wifis:
                all-wlans:
                    access-points:
                        Joe's home:
                            password: s3kr1tP4ss
                wlp1s0:
                    access-points:
                        guest:
                            mode: ap
            bridges:
                br0:
                    interfaces:
                    - wlp1s0
                    - switchports
                    dhcp4: true
      NETPLAN
    )
  end
end
