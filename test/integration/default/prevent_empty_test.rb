# frozen_string_literal: true

# Chef InSpec test for recipe netplan.io::default

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

# The netplan package should be installed
describe package('netplan.io') do
  it { should be_installed }
end

describe file('/etc/netplan/50-cloud-init.yaml') do
  it { should exist }
  its('content') do
    should match(
      <<~NETPLAN
        network:
            renderer: NetworkManager
            ethernets:
                ens3:
                    dhcp4: true
                    match:
                        macaddress: fa:16:3e:f5:09:19
                    set-name: ens3
            version: 2
      NETPLAN
    )
  end
end
