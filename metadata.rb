# frozen_string_literal: true

name              'netplan_io'
maintainer        'Pharmony SA'
maintainer_email  'dev@pharmony.lu'
license           'MIT'
description       'Installs and configure Ubuntu netplan package'
version           '0.4.0'
chef_version      '>= 13.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/pharmony/netplan.io-cookbook/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/pharmony/netplan.io-cookbook'

supports 'ubuntu'
